import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:cached_network_image/cached_network_image.dart';
import './util.dart';

// to view image in full screen
class GalleryImageViewWrapper extends StatefulWidget {
  final LoadingBuilder? loadingBuilder;
  final BoxDecoration? backgroundDecoration;
  final int? initialIndex;
  final PageController pageController;
  final Axis scrollDirection;
  final List<String> imageUrls;
  final String title;
  final List<String> captions;

  GalleryImageViewWrapper({
    this.loadingBuilder,
    this.backgroundDecoration,
    this.initialIndex,
    this.scrollDirection = Axis.horizontal,
    required this.imageUrls,
    required this.title,
    required this.captions,
  }) : pageController = PageController(initialPage: initialIndex ?? 0);

  @override
  State<StatefulWidget> createState() {
    return _GalleryImageViewWrapperState();
  }
}

class _GalleryImageViewWrapperState extends State<GalleryImageViewWrapper> {
  final minScale = PhotoViewComputedScale.contained * 0.8;
  final maxScale = PhotoViewComputedScale.covered * 8;
  int pageNumber = 0;
  @override
  void initState() {
    super.initState();
    widget.pageController.addListener(() {
      setState(() {
        pageNumber = (widget.pageController.page ?? 0).toInt();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: TextStyle(
            fontFamily: '--apple-system',
          ),
        ),
      ),
      body: Container(
        decoration: widget.backgroundDecoration,
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
        child: Stack(
          children: <Widget>[
            pageNumber <= 0
                ? getEmptyWidget()
                : GestureDetector(
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                        size: MediaQuery.of(context).size.width / 12,
                        semanticLabel: 'Previous',
                      ),
                    ),
                    onTap: () {
                      widget.pageController.animateToPage(
                        pageNumber - 1,
                        curve: Curves.ease,
                        duration: const Duration(milliseconds: 250),
                      );
                    },
                  ),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width / 11.5),
              alignment: Alignment.center,
              child: PhotoViewGallery.builder(
                scrollPhysics: const BouncingScrollPhysics(),
                builder: _buildImage,
                itemCount: widget.imageUrls.length,
                loadingBuilder: widget.loadingBuilder,
                backgroundDecoration: widget.backgroundDecoration,
                pageController: widget.pageController,
                scrollDirection: widget.scrollDirection,
              ),
            ),
            pageNumber + 1 >= widget.imageUrls.length
                ? getEmptyWidget()
                : GestureDetector(
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: Icon(
                        Icons.arrow_forward,
                        color: Colors.white,
                        size: MediaQuery.of(context).size.width / 12,
                        semanticLabel: 'Next',
                      ),
                    ),
                    onTap: () {
                      widget.pageController.animateToPage(
                        pageNumber + 1,
                        curve: Curves.ease,
                        duration: const Duration(milliseconds: 250),
                      );
                    },
                  )
          ],
        ),
      ),
    );
  }

// build image with zooming
  PhotoViewGalleryPageOptions _buildImage(BuildContext context, int index) {
    return PhotoViewGalleryPageOptions.customChild(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            CachedNetworkImage(
              height: MediaQuery.of(context).size.height / 2,
              imageUrl: widget.imageUrls[index],
              placeholder: (context, url) =>
                  Center(child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            Expanded(
              child: Center(
                child: Text(
                  widget.captions[index],
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25.0,
                    fontFamily: '--apple-system',
                  ),
                ),
              ),
              flex: 0,
            ),
          ],
        ),
      ),
      initialScale: PhotoViewComputedScale.contained,
      minScale: minScale,
      maxScale: maxScale,
      heroAttributes: PhotoViewHeroAttributes(tag: index),
    );
  }
}
