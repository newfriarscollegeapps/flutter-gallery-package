library galleryimage;

import 'package:flutter/material.dart';
import './util.dart';

class GalleryImage extends StatefulWidget {
  final List<String> imageUrls;
  final String? titleGallery;

  const GalleryImage({required this.imageUrls, this.titleGallery});
}

class _GalleryImageState extends State<GalleryImage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return getEmptyWidget();
  }
}
